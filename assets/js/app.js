// Initialize Masonry
var $grid = $('.row').masonry({
    percentPosition: true
});
// Layout Masonry after each image loads
$grid.imagesLoaded().progress(function() {
    $grid.masonry('layout');
});

function search(str) {
    if (str.length == 0) {
        $("#temporaryBanner").show();
        $("#searchResults").empty();
        return;
    } else {
        $("#temporaryBanner").hide();
        $("#searchResults").empty();
        for (var i = 0; i < data.length; i++) {
            var title = data[i].title;
            if (title.toUpperCase().includes(str.toUpperCase())) {
                var url = data[i].url;
                var minified = data[i].minified;
                var item = "<div class='col-sm-6 col-lg-4 mb-4'><div class='card'><img class='card-img-top h-100 w-100' src='" + minified + "' alt='" + title + "'><a data-fancybox='gallery' aria-label='Open gallery viewer' href='" + url + "'><div class='card-body'><h5 class='card-title'>" + title + "</h5></div></a></div></div>";
                $("#searchResults").append(item);
            }
        }
    }
}

var data = [{
        "title": "Rudolph's Hoe Mart",
        "url": "https://img.cleberg.io/photos/20140530.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20140530.jpg"
    },
    {
        "title": "California Seals",
        "url": "https://img.cleberg.io/photos/20140601.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20140601.jpg"
    },
    {
        "title": "Devil's Tower",
        "url": "https://img.cleberg.io/photos/20140716.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20140716.jpg"
    },
    {
        "title": "Bear Country, South Dakota",
        "url": "https://img.cleberg.io/photos/20140717.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20140717.jpg"
    },
    {
        "title": "Pactola Lake",
        "url": "https://img.cleberg.io/photos/20140718.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20140718.jpg"
    },
    {
        "title": "Welcome to Utah: Part 1",
        "url": "https://img.cleberg.io/photos/20190608_01.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190608_01.jpg"
    },
    {
        "title": "Welcome to Utah: Part 2",
        "url": "https://img.cleberg.io/photos/20190608_02.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190608_02.jpg"
    },
    {
        "title": "Canyonlands: Part 1",
        "url": "https://img.cleberg.io/photos/20190608_03.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190608_03.jpg"
    },
    {
        "title": "Canyonlands: Part 2",
        "url": "https://img.cleberg.io/photos/20190608_04.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190608_03.jpg"
    },
    {
        "title": "Canyonlands: Part 3",
        "url": "https://img.cleberg.io/photos/20190608_05.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190608_05.jpg"
    },
    {
        "title": "Canyonlands: Part 4",
        "url": "https://img.cleberg.io/photos/20190608_06.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190608_06.jpg"
    },
    {
        "title": "Kolob Canyon",
        "url": "https://img.cleberg.io/photos/20190609.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190609.jpg"
    },
    {
        "title": "Red Rock Canyon: Part 1",
        "url": "https://img.cleberg.io/photos/20190610_01.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190610_01.jpg"
    },
    {
        "title": "Red Rock Canyon: Part 2",
        "url": "https://img.cleberg.io/photos/20190610_02.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190610_02.jpg"
    },
    {
        "title": "Hoover Dam",
        "url": "https://img.cleberg.io/photos/20190611_01.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190611_01.jpg"
    },
    {
        "title": "Lake Mead",
        "url": "https://img.cleberg.io/photos/20190611_02.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190611_02.jpg"
    },
    {
        "title": "Honey Badger",
        "url": "https://img.cleberg.io/photos/20190611_03.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190611_03.jpg"
    },
    {
        "title": "Grand Canyon Lizard",
        "url": "https://img.cleberg.io/photos/20190612.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20190612.jpg"
    },
    {
        "title": "The Workhouse: Part 1",
        "url": "https://img.cleberg.io/photos/20191116_01.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20191116_01.jpg"
    },
    {
        "title": "The Workhouse: Part 2",
        "url": "https://img.cleberg.io/photos/20191116_02.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20191116_02.jpg"
    },
    {
        "title": "The Workhouse: Part 3",
        "url": "https://img.cleberg.io/photos/20191116_03.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20191116_03.jpg"
    },
    {
        "title": "The Workhouse: Part 4",
        "url": "https://img.cleberg.io/photos/20191116_04.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20191116_04.jpg"
    },
    {
        "title": "The Workhouse: Part 5",
        "url": "https://img.cleberg.io/photos/20191116_05.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20191116_05.jpg"
    },
    {
        "title": "The Workhouse: Part 6",
        "url": "https://img.cleberg.io/photos/20191116_06.jpg",
        "minified": "https://img.cleberg.io/photos/thumbnails/tn_20191116_06.jpg"
    }
]
