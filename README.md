# Photos
Photos is a dark-themed photography website with an adaptive, responsive layout and integrated gallery feature (on-click).

## Libraries
This site utilizes the following libraries:
- [Bootstrap v5.0](https://v5.getbootstrap.com/)
- [jQuery v3.5.1](https://code.jquery.com/)
- [Masonry v4.2.2](https://masonry.desandro.com/)
- [ImagesLoaded v4.1.4](https://imagesloaded.desandro.com/)
- [Fancybox v3](https://fancyapps.com/fancybox/3/)
